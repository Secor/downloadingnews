﻿using System;
using System.Collections.Generic;
using System.Threading;
using downloadNewsGdansk.Pages;

namespace downloadNewsGdansk
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CheckAllPages(5, 1);
            Console.ReadKey();
        }

        public static void CheckAllPages(int numberOfChecks, int minutesBetweenChecks)
        {
            var zarzadzeniaWojewody = new ZarzadzeniaWojewody();
            var dziennikUrzedowy = new DziennikUrzedowy();
            var newsUmGdansk = new NewsUmGdansk();
            var newsGdansk = new NewsGdansk();
            var newsWpGdansk = new NewsWpGdansk();
            var newsTrojmiasto = new NewsTrojmiasto();
            var onetTrojmiasto = new OnetTrojmiasto();
            for (var i = 1; i <= numberOfChecks; i++)
            {
                zarzadzeniaWojewody.CheckPage(2);
                dziennikUrzedowy.CheckPage(2);
                    PrintNewsList(dziennikUrzedowy.GetAddedNews());
                newsUmGdansk.CheckPage(2);
                newsGdansk.CheckPage(2);
                newsWpGdansk.CheckPage(2);
                newsTrojmiasto.CheckPage(2);
                //onetTrojmiasto.CheckPage(2);
                Console.WriteLine("\nSprawdzenie: " + i +"/" + numberOfChecks + ", czekamy " + minutesBetweenChecks + "min");
                Thread.Sleep(TimeSpan.FromMinutes(minutesBetweenChecks));
            }
        }

        private static void PrintNewsList(List<News> newsList)
        {
            foreach (var news in newsList)
            {
                Console.WriteLine("\n\nTitle: " + news.Title);
                Console.WriteLine("Date: " + news.Date);
                Console.WriteLine("Link/ID: " + news.Link);
                Console.WriteLine("Content: " + news.Content.Length);
                //Console.WriteLine("Content: " + news.Content);
                Console.WriteLine("----------");
            }
        }
    }
}
