﻿using System;

namespace downloadNewsGdansk
{
    public class News
    {
        public String Title;
        public String Date;
        public String Link;
        public String Content;

        public News() { }

        public News(string title, string date, string link, string content)
        {
            Title = title;
            Date = date;
            Link = link;
            Content = content;
        }
    }
}
