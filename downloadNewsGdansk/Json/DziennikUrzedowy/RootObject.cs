﻿using System.Collections.Generic;

namespace downloadNewsGdansk.Json.DziennikUrzedowy
{
    class RootObject
    {
        public List<object> Tabs { get; set; }
        public List<object> JournalNumbersList { get; set; }
        public bool HasJournal { get; set; }
        public List<Position> Positions { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
    }
}
