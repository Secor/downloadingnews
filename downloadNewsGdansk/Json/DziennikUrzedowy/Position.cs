﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace downloadNewsGdansk.Json.DziennikUrzedowy
{
    class Position
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public string PublicationDate { get; set; }
        public string ActDate { get; set; }
        public string PdfUrl { get; set; }
        public string Subject { get; set; }
        public string Publisher { get; set; }
        public string CaseNumber { get; set; }
        public bool HasExpired { get; set; }
        public string LegalActType { get; set; }
        public int ActTypeId { get; set; }
        public List<PublishersList> PublishersList { get; set; }
        public string PublishersListFlat { get; set; }
        public string Title { get; set; }
        public int Oid { get; set; }
        [JsonProperty("Position")]
        public int Index { get; set; }
        public int Year { get; set; }
        public object DuplicateChar { get; set; }
        public int JournalNumber { get; set; }
    }
}
