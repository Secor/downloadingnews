﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace downloadNewsGdansk
{
    public class XmlDbManager
    {
        private readonly string _dbFileName;

        public XmlDbManager(string fileName)
        {
            var basePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            _dbFileName = Path.Combine(basePath, fileName);
        }

        public void AppendNews(List<News> newsFromWeb)
        {
            if (File.Exists(_dbFileName))
            {
                var newsFromFile = GetNews();
                newsFromFile.AddRange(newsFromWeb);
                Save(newsFromFile);
            }
            else
            {
                SaveNews(newsFromWeb);
            }
        }

        private void Save(List<News> newsList)
        {
            var writer = new XmlSerializer(typeof(List<News>));
            var file = File.Open(_dbFileName, FileMode.Create);
            writer.Serialize(file, newsList);
            file.Close();
        }

        private void SaveNews(List<News> news)
        {
            var writer = new XmlSerializer(typeof(List<News>));
            var file = File.Create(_dbFileName);
            writer.Serialize(file, news);
            file.Close();
        }

        public List<string> GetLinks()
        {
            var linkList = new List<string>();
            if (File.Exists(_dbFileName))
            {
                var newsFromFile = GetNews();
                foreach (var news in newsFromFile)
                {
                    linkList.Add(news.Link);
                }
            }
            return linkList;
        }

        public List<News> GetNews()
        {
            var newsFromFile = new List<News>();
            if (File.Exists(_dbFileName))
            {
                var writer = new XmlSerializer(typeof(List<News>));
                var file = File.Open(_dbFileName, FileMode.Open);
                var reader = XmlReader.Create(file);
                newsFromFile = (List<News>)writer.Deserialize(reader);
                file.Close();
            }
            return newsFromFile;
        }
    }
}
