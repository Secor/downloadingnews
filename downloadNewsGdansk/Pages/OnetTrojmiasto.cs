﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace downloadNewsGdansk.Pages
{
    class OnetTrojmiasto : CommonGdansk
    {
        public override void CheckPage(int quantityLastMessages)
        {
            Console.WriteLine("\nSprawdzam wpisy onet.pl Trojmiasto: ");
            var xmlDbManager = new XmlDbManager("News.xml");
            var linkList = xmlDbManager.GetLinks();
            var hrefCollection = GetHrefCollection(quantityLastMessages);
            var newsList = new List<News>();
            var news = new News();
            foreach (var link in hrefCollection)
            {
                if (!linkList.Contains(link))
                {
                    //news = ReadLink(link);
                    if(news.Title != null && news.Content != null)
                        newsList.Add(news);
                }
            }
            if (newsList.Count > 0)
            {
                xmlDbManager.AppendNews(newsList);
                Console.WriteLine("Liczba dodanych newsów: " + newsList.Count);
            }
        }

        public override List<string> GetHrefCollection(int quantityLastMessages)
        {
            const string url = "http://wiadomosci.onet.pl/trojmiasto";
            var htmlWeb = new HtmlWeb();
            var selectedLinks = new List<string>();     
            try
            {
                var htmlDocument = htmlWeb.Load(url);
                var hrefsCollection = htmlDocument.DocumentNode.SelectNodes("//div[@class='listItem listItemSolr itArticle']//a[@href]");
                foreach (var node in hrefsCollection)
                {
                    var att = node.Attributes["href"];
                    if (att.Value.Length > 47)  //TODO 47 = "http://wiadomosci.onet.pl/trojmiasto/ekstraliga" longest from collection before appropriate link
                        selectedLinks.Add(att.Value);
                    if (selectedLinks.Count == quantityLastMessages) break;
                }
            }
            catch (WebException)
            {
                Console.WriteLine("Problem z połączeniem " + url);
            }
            return selectedLinks;
        }
    }
}
