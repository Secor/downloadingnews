﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;

namespace downloadNewsGdansk.Pages
{
    class NewsTrojmiasto : CommonGdansk
    {
        public override void CheckPage(int quantityLastMessages)
        {
            Console.WriteLine("\nSprawdzam wpisy Trojmiasto: ");
            var xmlDbManager = new XmlDbManager("News.xml");
            var linkList = xmlDbManager.GetLinks();
            var hrefCollection = GetHrefCollection(quantityLastMessages);
            var newsList = new List<News>();
            var news = new News();
            foreach (var link in hrefCollection)
            {
                if (!linkList.Contains(link))
                {
                    news = ReadLink(link);
                    if(news.Title != null && news.Content != null)
                        newsList.Add(news);
                }
            }
            if (newsList.Count > 0)
            {
                xmlDbManager.AppendNews(newsList);
                Console.WriteLine("Liczba dodanych newsów: " + newsList.Count);
            }
        }

        public override List<string> GetHrefCollection(int quantityLastMessages)
        {
            const string url = "http://www.trojmiasto.pl/wiadomosci/fakty-i-opinie/wiadomosci/?string_news=&cat_type=%2Ffakty-i-opinie%2Fwiadomosci%2F&autor=&miasto[1]=1&id_district=0&dzienstart=&dzienstop=&id_watki=%2C%2C";
            var htmlWeb = new HtmlWeb();
            var selectedLinks = new List<string>();     
            try
            {
                var htmlDocument = htmlWeb.Load(url);
                var hrefsCollection = htmlDocument.DocumentNode.SelectNodes("//a[@class='color04'][@href]");
                foreach (var node in hrefsCollection)
                {
                    var att = node.Attributes["href"];
                    selectedLinks.Add(att.Value);
                    if (selectedLinks.Count == quantityLastMessages) break;
                }
            }
            catch (WebException)
            {
                Console.WriteLine("Problem z połączeniem " + url);
            }
            return selectedLinks;
        }

        public override News ReadLink(string newsLink)
        {
            var docHtml = new HtmlDocument();
            var docHFile = new HtmlWeb
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.GetEncoding("iso-8859-2")
            };
            var news = new News();
            try
            {
                docHtml = docHFile.Load(newsLink);
                news.Title = docHtml.DocumentNode.SelectSingleNode("//div[@class='text-container']//h1").InnerText.Trim();
                news.Link = newsLink;
                var date = WebUtility.HtmlDecode(docHtml.DocumentNode.SelectSingleNode("//div[@class='text-container']//p[@class='article-info']/text()").InnerText);
                var dateTime = DateTimeParseTrojmiasto(date);
                news.Date = dateTime.Day + "." + dateTime.Month + "." + dateTime.Year;    
                var content = string.Join("",
                    docHtml.DocumentNode.SelectSingleNode("//div[@id='article-content']")
                        .ChildNodes.Where(cn => cn.NodeType == HtmlNodeType.Text)
                        .Select(n => n.InnerText));
                content = WebUtility.HtmlDecode(content).Trim();
                var summary = WebUtility.HtmlDecode(docHtml.DocumentNode.SelectSingleNode("//div[@id='article-content']//p[@class='lead']").InnerText).Trim();
                news.Content = summary + " " + content;
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("\nProblem z linkiem = " + newsLink);
            }
            catch (HtmlWebException)
            {
                Debug.WriteLine("\nProblem z rozpoznaniem strony" + newsLink);
            }
            catch (Exception e)
            {
                Debug.WriteLine("\nNewsTrojmiasto: " + e);
            }
            return news;
        }

        private static DateTime DateTimeParseTrojmiasto(string strDate)
        {
            var day = 0;
            var month = 0;
            var year = 0;
            var monthStr = "";
            //string strDate = "24 marca 2016, godz. 11:30 ("; //Debug example
            var charDate = strDate.ToCharArray();
            day = (int)char.GetNumericValue(charDate[0]) * 10 + (int)char.GetNumericValue(charDate[1]);
            var i = 3;
            while (charDate[i] != ' ')
            {
                monthStr += charDate[i];
                i++;
            }
            var monthArr = new string[] { "", "stycznia", "lutego", "marca", "kwietnia" };
            month = Array.IndexOf(monthArr, monthStr);
            year = (int)char.GetNumericValue(charDate[i + 1]) * 1000 + (int)char.GetNumericValue(charDate[i + 2]) * 100;
            year += (int)char.GetNumericValue(charDate[i + 3]) * 10 + (int)char.GetNumericValue(charDate[i + 4]);
            var generatedDate = new DateTime(year, month, day);
            return generatedDate;
        }
    }
}
