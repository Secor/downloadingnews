﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using downloadNewsGdansk.Json.DziennikUrzedowy;
using Newtonsoft.Json;

namespace downloadNewsGdansk.Pages
{
    class DziennikUrzedowy : CommonGdansk
    {
        private List<News> _addedNews = new List<News>();

        public override void CheckPage(int quantityLastMessages)
        {
            Console.WriteLine("\nSprawdzam wpisy DziennikUrzedowy: ");
            var newsList = new List<News>();
            var xmlDbManager = new XmlDbManager("News.xml");
            var linkList = xmlDbManager.GetLinks();
            try
            {
                var newsListFromJson = GetNewsCollection(quantityLastMessages);
                for (var i = 0; i <= quantityLastMessages - 1; i++)
                {
                    if (!linkList.Contains(newsListFromJson[i].Link))
                    {
                        newsList.Add(newsListFromJson[i]);
                    }
                }
                if (newsList.Count > 0)
                {
                    _addedNews = newsList;
                    xmlDbManager.AppendNews(newsList);
                    Console.WriteLine("Liczba dodanych newsów: " + newsList.Count);
                }
            }
            catch (WebException)
            {
                Console.WriteLine("Problem z polaczeniem http://edziennik.gdansk.uw.gov.pl");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public List<News> GetAddedNews()
        {
            return _addedNews;
        } 

        public override List<News> GetNewsCollection(int quantityLastMessages)
        {
            var newsList = new List<News>();
            var rootObject = GetRootObject();
            List<Position> positionsList = rootObject.Positions;
            var sortedList = positionsList.OrderByDescending(a => a.PublicationDate).ToList();
            for (var i = 0; i <= quantityLastMessages - 1; i++)
            {
                var date = sortedList[i].PublicationDate.Remove(10);
                date = date.Substring(8, 2) + "." + date.Substring(5, 2) + "." + date.Substring(0, 4);
                var news = new News(sortedList[i].Title, date, "http://edziennik.gdansk.uw.gov.pl/" + sortedList[i].PublicationDate, sortedList[i].Subject);
                newsList.Add(news);
            }
            return newsList;
        }

        private static RootObject GetRootObject()
        {
            var client = new WebClient();
            client.Encoding = Encoding.UTF8;
            var json = client.DownloadString("http://edziennik.gdansk.uw.gov.pl/api/positions?year=2016&month=0&isList=true");
            var rootObject = JsonConvert.DeserializeObject<RootObject>(json);
            return rootObject;
        }
    }
}
