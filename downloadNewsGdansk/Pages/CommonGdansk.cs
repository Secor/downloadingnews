﻿using System;
using System.Collections.Generic;

namespace downloadNewsGdansk.Pages
{
    class CommonGdansk
    {
        public virtual void CheckPage(int quantityLastMessages) {   }

        public virtual List<string> GetHrefCollection(int quantityLastMessages)
        {
            return new List<string>();
        }

        public virtual List<News> GetNewsCollection(int quantityLastMessages)
        {
            return new List<News>();
        }

        public virtual News ReadLink(string newsLink)
        {
            return new News();
        }

        public static DateTime DateTimeParse(string strDate)
        {
            var day = 0;
            var month = 0;
            var year = 0;
            var monthStr = "";
            //string strDate = "data publikacji: 04 marca 2016 r"; //Debug example
            var charDate = strDate.ToCharArray();
            day = (int)char.GetNumericValue(charDate[17]) * 10 + (int)char.GetNumericValue(charDate[18]);
            var i = 20;
            while (charDate[i] != ' ')
            {
                monthStr += charDate[i];
                i++;
            }
            var monthArr = new string[] { "", "stycznia", "lutego", "marca", "kwietnia" };
            month = Array.IndexOf(monthArr, monthStr);
            year = (int)char.GetNumericValue(charDate[i + 1]) * 1000 + (int)char.GetNumericValue(charDate[i + 2]) * 100;
            year += (int)char.GetNumericValue(charDate[i + 3]) * 10 + (int)char.GetNumericValue(charDate[i + 4]);
            var generatedDate = new DateTime(year, month, day);
            return generatedDate;
        }
    }
}
