﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace downloadNewsGdansk.Pages
{
    class NewsWpGdansk : CommonGdansk
    {
        public override void CheckPage(int quantityLastMessages)
        {
            Console.WriteLine("\nSprawdzam wpisy Wirtualna Polska Gdańsk: ");
            var xmlDbManager = new XmlDbManager("News.xml");
            var linkList = xmlDbManager.GetLinks();
            var hrefCollection = GetHrefCollection(quantityLastMessages);
            var newsList = new List<News>();
            var news = new News();
            foreach (var link in hrefCollection)
            {
                if (!linkList.Contains(link))
                {
                    news = ReadLink(link);
                    if(news.Title != null && news.Content != null)
                        newsList.Add(news);
                }
            }
            if (newsList.Count > 0)
            {
                xmlDbManager.AppendNews(newsList);
                Console.WriteLine("Liczba dodanych newsów: " + newsList.Count);
            }
        }

        public override List<string> GetHrefCollection(int quantityLastMessages)
        {
            const string url = "http://wiadomosci.wp.pl/query,gda%F1sk,szukaj.html?ticaid=116b3c";
            var htmlWeb = new HtmlWeb();
            var selectedLinks = new List<string>();     
            try
            {
                var htmlDocument = htmlWeb.Load(url);
                var hrefsCollection = htmlDocument.DocumentNode.SelectNodes("//div[@id='bxWyszukiwanie']//li[@class='first']//a[@class='ikonka'][@href]");
                foreach (var node in hrefsCollection)
                {
                    var att = node.Attributes["href"];
                    selectedLinks.Add(att.Value);
                    if (selectedLinks.Count == quantityLastMessages) break;
                }
            }
            catch (WebException)
            {
                Console.WriteLine("Problem z połączeniem " + url);
            }
            return selectedLinks;
        }

        public override News ReadLink(string newsLink)
        {
            var docHtml = new HtmlDocument();
            var docHFile = new HtmlWeb
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.GetEncoding("iso-8859-2")
            };
            var news = new News();
            try
            {
                docHtml = docHFile.Load(newsLink);
                news.Title = WebUtility.HtmlDecode(docHtml.DocumentNode.SelectSingleNode("//div[@class='h1']").InnerText);
                news.Link = newsLink;
                var dateFromPage = docHtml.DocumentNode.SelectSingleNode("//div[@class='autorDesc']//time");
                var att = dateFromPage.Attributes["datetime"];
                var date = DateTime.Parse(att.Value);
                news.Date = date.Day + "." + date.Month + "." + date.Year;
                var summary = WebUtility.HtmlDecode(docHtml.DocumentNode.SelectSingleNode("//main[@class='ST-Artykul']//div[@class='lead']").InnerText);
                var content = string.Join("",
                    docHtml.DocumentNode.SelectSingleNode("//main[@class='ST-Artykul']//div[@id='intertext1']")
                        .ChildNodes.Where(cn => cn.NodeType == HtmlNodeType.Text)
                        .Select(n => n.InnerText));
                news.Content = Regex.Replace(summary + " " + content, @"\t|\n|\r", ""); ;
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("\nProblem z linkiem = " + newsLink);
            }
            catch (HtmlWebException)
            {
                Debug.WriteLine("\nProblem z rozpoznaniem strony" + newsLink);
            }
            catch (Exception e)
            {
                Debug.WriteLine("\nNewsWPGdansk: " + e);
            }
            return news;
        }
    }
}
