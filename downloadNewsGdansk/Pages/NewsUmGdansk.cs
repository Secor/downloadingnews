﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using HtmlAgilityPack;

namespace downloadNewsGdansk.Pages
{
    class NewsUmGdansk : CommonGdansk
    {
        public override void CheckPage(int quantityLastMessages)
        {
            Console.WriteLine("\nSprawdzam wpisy UM GDA: ");
            var xmlDbManager = new XmlDbManager("News.xml");
            var linkList = xmlDbManager.GetLinks();
            var hrefCollection = GetHrefCollection(quantityLastMessages);
            var newsList = new List<News>();
            var news = new News();
            foreach (var link in hrefCollection)
            {
                if (!linkList.Contains(link))
                {
                    news = ReadLink(link);
                    newsList.Add(news);
                }
            }
            if (newsList.Count > 0)
            {
                xmlDbManager.AppendNews(newsList);
                Console.WriteLine("Liczba dodanych newsów: " + newsList.Count);
            }
        }

        public override List<string> GetHrefCollection(int quantityLastMessages)
        {
            const string url = "http://www.gdansk.pl/urzad-miejski/Wiadomosci,a,512";
            var htmlWeb = new HtmlWeb();
            var selectedLinks = new List<string>();
            try
            {
                var htmlDocument = htmlWeb.Load(url);
                var hrefsCollection = htmlDocument.DocumentNode.SelectNodes("//div[@class='block news-7']//a[@href]");
                foreach (var node in hrefsCollection)
                {
                    var att = node.Attributes["href"];
                    selectedLinks.Add(att.Value);
                    if (selectedLinks.Count == quantityLastMessages) break;
                }
            }
            catch (WebException)
            {
                Console.WriteLine("Problem z połączeniem " + url);
            }
            return selectedLinks;
        }

        public override News ReadLink(string newsLink)
        {
            var docHtml = new HtmlDocument();
            var docHFile = new HtmlWeb();
            var news = new News();
            try
            {
                docHtml = docHFile.Load(newsLink);
                news.Title = docHtml.DocumentNode.SelectSingleNode("//div[@class='col-md-8 article-news']//h1").InnerText;
                news.Link = newsLink;
                var date = DateTimeParse(docHtml.DocumentNode.SelectSingleNode("//div[@class='article-news-date']").InnerText);
                news.Date = date.Day + "." + date.Month + "." + date.Year;               
                var pCollection = docHtml.DocumentNode.SelectNodes("//div[@class='article-news-content fontSize']//p");
                news.Content = docHtml.DocumentNode.SelectSingleNode("//div[@class='col-md-8 article-news']//h3").InnerText + " ";
                foreach (var node in pCollection)
                {
                    news.Content += node.InnerText + " ";
                }
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("\nProblem z linkiem = " + newsLink);
            }
            catch (HtmlWebException)
            {
                Debug.WriteLine("\nProblem z rozpoznaniem strony" + newsLink);
            }
            catch (Exception e)
            {
                Debug.WriteLine("\nReadNewsUmGdansk: " + e);
            }
            return news;
        }
    }
}
