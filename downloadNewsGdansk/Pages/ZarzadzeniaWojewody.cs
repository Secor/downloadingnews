﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace downloadNewsGdansk.Pages
{
    class ZarzadzeniaWojewody : CommonGdansk
    {
        public override void CheckPage(int quantityLastMessages)
        {
            Console.WriteLine("\nSprawdzam wpisy ZarzadzeniaWojewody: ");
            var xmlDbManager = new XmlDbManager("News.xml");
            var linkList = xmlDbManager.GetLinks();
            var hrefCollection = GetHrefCollection(quantityLastMessages);
            var newsList = new List<News>();
            var news = new News();
            foreach (var link in hrefCollection)
            {
                if (!linkList.Contains(link))
                {
                    news = ReadLink(link);
                    if (news.Title != null && news.Content != null)
                        newsList.Add(news);
                }
            }
            if (newsList.Count > 0)
            {
                xmlDbManager.AppendNews(newsList);
                Console.WriteLine("Liczba dodanych newsów: " + newsList.Count);
            }
        }

        public override List<string> GetHrefCollection(int quantityLastMessages)
        {
            const string url = "http://uwgdansk.ssdip.bip.gov.pl/2016-rok/";
            var htmlWeb = new HtmlWeb();
            var selectedLinks = new List<string>();     
            try
            {
                var htmlDocument = htmlWeb.Load(url);
                var hrefsCollection = htmlDocument.DocumentNode.SelectNodes("//div[@class='art_list']//div[@class='lewy']//a[@href]");
                foreach (var node in hrefsCollection)
                {
                    var att = node.Attributes["href"];
                    selectedLinks.Add("http://uwgdansk.ssdip.bip.gov.pl" + att.Value);
                    if (selectedLinks.Count == quantityLastMessages) break;
                }
            }
            catch (WebException)
            {
                Console.WriteLine("Problem z połączeniem " + url);
            }
            return selectedLinks;
        }

        public override News ReadLink(string newsLink)
        {
            var docHtml = new HtmlDocument();
            var docHFile = new HtmlWeb();
            var news = new News();
            try
            {
                docHtml = docHFile.Load(newsLink);
                var title = docHtml.DocumentNode.SelectSingleNode("//div[@id='widok_art']//h2").InnerText;
                news.Title = Regex.Replace(title, @"\t|\n|\r", "");
                news.Link = newsLink;
                var date = docHtml.DocumentNode.SelectSingleNode("//div[@class='data_akcje']").InnerText;
                date = date.Trim();
                date = date.Remove(10);
                news.Date = date.Substring(8, 2) + "." + date.Substring(5, 2) + "." + date.Substring(0, 4);
                var content = WebUtility.HtmlDecode(docHtml.DocumentNode.SelectSingleNode("//div[@id='widok_art']//span").InnerText);
                news.Content = Regex.Replace(content, @"\t|\n|\r", "");                
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("\nProblem z linkiem = " + newsLink);
            }
            catch (HtmlWebException)
            {
                Debug.WriteLine("\nProblem z rozpoznaniem strony" + newsLink);
            }
            catch (Exception e)
            {
                Debug.WriteLine("\nRead NewsZarzadzeniaWojewody: " + e);
            }
            return news;
        }
    }
}
